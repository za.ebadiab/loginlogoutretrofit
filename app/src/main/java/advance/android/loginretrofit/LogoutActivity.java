package advance.android.loginretrofit;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import advance.android.loginretrofit.MVPLogout.Contracts;
import advance.android.loginretrofit.MVPLogout.Presenter;
import advance.android.loginretrofit.utils.PublicMethods;

public class LogoutActivity extends AppCompatActivity implements Contracts.View {


    Presenter presenter = new Presenter(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logout);
        bind();
    }

    void bind() {

        findViewById(R.id.btnLogout).setOnClickListener(v -> {
            presenter.logout();
        });
    }

    @Override
    public void logoutSuccess() {
        PublicMethods.toast("Logout successed!");
        Intent intent = new Intent(this, ActivityMain.class);
        startActivity(intent);
    }

    @Override
    public void logoutFailed() {
        PublicMethods.toast("Logout Failed!");
    }
}
