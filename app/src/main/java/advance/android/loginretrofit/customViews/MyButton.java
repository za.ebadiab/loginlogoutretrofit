package advance.android.loginretrofit.customViews;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

import advance.android.loginretrofit.utils.BaseApplication;

public class MyButton extends AppCompatButton {
    public MyButton(Context context) {
        super(context);
        this.setTypeface(BaseApplication.typeface);

    }

    public MyButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(BaseApplication.typeface);
    }
}
