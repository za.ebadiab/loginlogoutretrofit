package advance.android.loginretrofit.MVPLogout;

public class Presenter implements Contracts.Presenter {
    Contracts.Model model = new Model();

    Contracts.View view;

    public Presenter(Contracts.View view) {
        model.attachPresenter(this);
        this.view = view;

    }

    @Override
    public void logout() {
        model.logout();
    }

    @Override
    public void logoutSuccess() {
        view.logoutSuccess();
    }

    @Override
    public void logoutFailed() {
        view.logoutFailed();
    }
}
