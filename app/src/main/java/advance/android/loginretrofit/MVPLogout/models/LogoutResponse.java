package advance.android.loginretrofit.MVPLogout.models;

import com.google.gson.annotations.SerializedName;

public class LogoutResponse{

	@SerializedName("action")
	private String action;


	public void setAction(String action){
		this.action = action;
	}

	public String getAction(){
		return action;
	}

	@Override
	public String toString(){
		return
				"Response2{" +
						",action = '" + action + '\'' +
						"}";
	}
}