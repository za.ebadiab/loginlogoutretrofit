package advance.android.loginretrofit.MVPLogout;

public class Contracts {

    public interface View {

        void logoutSuccess();

        void logoutFailed();
    }

    interface Presenter {

        void logout();

        void logoutSuccess();

        void logoutFailed();
    }

    interface Model {

        void attachPresenter(Presenter presenter);

        void logout();

    }

}
