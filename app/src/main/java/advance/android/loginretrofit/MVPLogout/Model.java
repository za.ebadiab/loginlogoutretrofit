package advance.android.loginretrofit.MVPLogout;


import advance.android.loginretrofit.MVPLogout.models.LogoutResponse;
import advance.android.loginretrofit.utils.PublicMethods;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Model implements Contracts.Model {
    Contracts.Presenter presenter;


    @Override
    public void attachPresenter(Contracts.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void logout() {

        PublicMethods.retrofitGenerator().logout(PublicMethods.loadData("token", "")).enqueue(new Callback<LogoutResponse>() {
            @Override
            public void onResponse(Call<LogoutResponse> call, Response<LogoutResponse> response) {

                if (response.body().getAction().equals("success")) {

                    presenter.logoutSuccess();
                } else {

                    presenter.logoutFailed();
                }

            }

            @Override
            public void onFailure(Call<LogoutResponse> call, Throwable t) {

                presenter.logoutFailed();
            }
        });


    }


}


