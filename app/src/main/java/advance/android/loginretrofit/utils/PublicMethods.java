package advance.android.loginretrofit.utils;

import android.widget.Toast;

import com.orhanobut.hawk.Hawk;

public class PublicMethods {


    public static void storeData(String key, String value) {
        Hawk.put(key, value);

    }

    public static String loadData(String key, String value) {
        return Hawk.get(key);

    }

    public static void toast(String msg) {
        Toast.makeText(BaseApplication.baseApp, msg, Toast.LENGTH_SHORT).show();

    }


    public static RetrofitInterface retrofitGenerator() {
        return RetrofitGenerator.createService(RetrofitInterface.class);

    }


}
