package advance.android.loginretrofit.utils;

import android.app.Application;
import android.graphics.Typeface;
import android.os.Debug;

import com.orhanobut.hawk.Hawk;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.BuildConfig;
import com.orhanobut.logger.Logger;


public class BaseApplication extends Application {
    public static Typeface typeface;
    public static BaseApplication baseApp;

    @Override
    public void onCreate() {
        super.onCreate();
        typeface = Typeface.createFromAsset(getAssets(), Constants.appFontName);
        baseApp = this;

        Hawk.init(this).build();


        Logger.addLogAdapter(new AndroidLogAdapter() {
            @Override
            public boolean isLoggable(int priority, String tag) {
                //     return BuildConfig.DEBUG ;
                return true;
            }
        });

    }
}


