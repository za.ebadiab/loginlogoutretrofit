package advance.android.loginretrofit.utils;

import advance.android.loginretrofit.MVPLogout.models.LogoutResponse;
import advance.android.loginretrofit.MVPLogin.models.TokenEntity;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface RetrofitInterface {



    @FormUrlEncoded
    @POST("login.php")
    Call<TokenEntity> login(
            @Field("username") String username ,
            @Field("password") String password
    ) ;

    @FormUrlEncoded
    @POST("logout.php")
    Call<LogoutResponse> logout(
           // @Header("token") String token ) ;
            @Field("token") String token ) ;



}
