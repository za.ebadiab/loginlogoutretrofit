package advance.android.loginretrofit;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import advance.android.loginretrofit.MVPLogin.Contracts;
import advance.android.loginretrofit.MVPLogin.Presenter;
import advance.android.loginretrofit.customViews.MyEditText;
import advance.android.loginretrofit.utils.PublicMethods;

public class ActivityMain extends AppCompatActivity implements Contracts.View {

    Presenter presenter = new Presenter(this);
    MyEditText txtUsername, txtPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bind();
    }

    void bind() {

        txtUsername = findViewById(R.id.txtUsername);
        txtPassword = findViewById(R.id.txtPassword);

        findViewById(R.id.btnLogin).setOnClickListener(v -> {

            presenter.login(txtUsername.text(), txtPassword.text());

        });
    }

    @Override
    public void loginSuccess(String token) {
        PublicMethods.toast("login successed. token= " + token);
        Intent intent = new Intent(this, LogoutActivity.class);
        startActivity(intent);
    }

    @Override
    public void loginFailed() {
        PublicMethods.toast("login failed");
    }
}
