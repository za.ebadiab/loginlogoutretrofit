package advance.android.loginretrofit.MVPLogin.models;

import com.google.gson.annotations.SerializedName;

public class TokenEntity {

    @SerializedName("action")
    private String action;

    @SerializedName("token")
    private String token;

    public void setAction(String action) {
        this.action = action;
    }

    public String getAction() {
        return action;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    @Override
    public String toString() {
        return
                "TokenEntity{" +
                        "action = '" + action + '\'' +
                        ",token = '" + token + '\'' +
                        "}";
    }
}