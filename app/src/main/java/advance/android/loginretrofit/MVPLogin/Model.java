package advance.android.loginretrofit.MVPLogin;

import advance.android.loginretrofit.MVPLogin.models.TokenEntity;
import advance.android.loginretrofit.utils.PublicMethods;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Model implements Contracts.Model {
    Contracts.Presenter presenter;


    @Override
    public void attachPresenter(Contracts.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void login(String username, String password) {
        PublicMethods.retrofitGenerator().login(username, password).enqueue(new Callback<TokenEntity>() {
            @Override
            public void onResponse(Call<TokenEntity> call, Response<TokenEntity> response) {

                if (response.body().getAction().equals("success")) {

                    presenter.loginSuccess(response.body().getToken());
                } else {

                    presenter.loginFailed();
                }

            }

            @Override
            public void onFailure(Call<TokenEntity> call, Throwable t) {

                presenter.loginFailed();
            }
        });
    }

    @Override
    public void storeToken(String token) {
        PublicMethods.storeData("token", token);
    }


}
