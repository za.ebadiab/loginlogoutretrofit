package advance.android.loginretrofit.MVPLogin;

public class Presenter implements Contracts.Presenter {

    Contracts.Model model = new Model();
    Contracts.View view;


    public Presenter(Contracts.View view) {
        this.view = view;
        model.attachPresenter(this);

    }

    @Override
    public void login(String username, String password) {
        model.login(username, password);
    }

    @Override
    public void loginSuccess(String token) {
        view.loginSuccess(token);
        model.storeToken(token);

    }

    @Override
    public void loginFailed() {
        view.loginFailed();
    }
}
