package advance.android.loginretrofit.MVPLogin;

public class Contracts {

    public interface View {

        void loginSuccess(String token);

        void loginFailed();
    }

    interface Presenter {


        void login(String username, String password);

        void loginSuccess(String token);

        void loginFailed();
    }

    interface Model {

        void attachPresenter(Presenter presenter);

        void login(String username, String password);

        void storeToken(String token);

    }

}
